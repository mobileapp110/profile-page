// ignore_for_file: prefer_const_constructors, prefer_const_literals_to_create_immutables, use_key_in_widget_constructors, avoid_unnecessary_containers, sized_box_for_whitespace

import 'package:flutter/material.dart';

enum APP_THEME { LIGHT, DARK }

void main() {
  runApp(ContactProfilePage());
}

class myAppTheme {
  static ThemeData appThemeLight() {
    return ThemeData(
        brightness: Brightness.light,
        appBarTheme: AppBarTheme(
            color: Color.fromARGB(255, 255, 131, 181),
            iconTheme: IconThemeData(color: Colors.white)),
        iconTheme: IconThemeData(color: Color.fromARGB(255, 255, 131, 181)),
        dividerTheme:
            DividerThemeData(color: Color.fromARGB(255, 255, 131, 181)));
  }

  static ThemeData appThemeDark() {
    return ThemeData(
        brightness: Brightness.dark,
        appBarTheme: AppBarTheme(
            color: Color.fromARGB(255, 221, 60, 124),
            iconTheme: IconThemeData(color: Colors.white)),
        dividerTheme:
            DividerThemeData(color: Color.fromARGB(255, 221, 60, 124)));
  }
}

class ContactProfilePage extends StatefulWidget {
  @override
  State<ContactProfilePage> createState() => _ContactProfilePageState();
}

class _ContactProfilePageState extends State<ContactProfilePage> {
  var currentTheme = APP_THEME.LIGHT;
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      theme: currentTheme == APP_THEME.LIGHT
          ? myAppTheme.appThemeLight()
          : myAppTheme.appThemeDark(),
      home: Scaffold(
        appBar: buildAppBarWidget(),
        body: buildBodyWidget(),
        floatingActionButton: FloatingActionButton(
          child: currentTheme == APP_THEME.DARK
              ? Icon(Icons.sunny)
              : Icon(Icons.nightlight),
          onPressed: () {
            setState(() {
              currentTheme == APP_THEME.DARK
                  ? currentTheme = APP_THEME.LIGHT
                  : currentTheme = APP_THEME.DARK;
            });
          },
        ),
      ),
    );
  }
}

AppBar buildAppBarWidget() {
  return AppBar(
    leading: Icon(
      Icons.arrow_back,
    ),
    actions: <Widget>[
      IconButton(
        onPressed: () {},
        icon: Icon(Icons.star_border),
      ),
    ],
  );
}

Widget buildBodyWidget() {
  return ListView(
    children: <Widget>[
      Column(
        children: <Widget>[
          Container(
            child: Image.network(
              "https://gitlab.com/webprogramming11/lab4/-/raw/main/manami.png",
              fit: BoxFit.cover,
            ),
          ),
          Container(
            height: 68,
            child: Row(
              mainAxisAlignment: MainAxisAlignment.start,
              children: <Widget>[
                Padding(
                  padding: EdgeInsets.all(10.0),
                  child: Text(
                    "Patchara L.",
                    style: TextStyle(
                        fontSize: 30,
                        color: Color.fromARGB(255, 255, 131, 181)),
                  ),
                )
              ],
            ),
          ),
          Divider(),
          Container(
            margin: EdgeInsets.only(top: 8, bottom: 8),
            child: Theme(
                data: ThemeData(
                    iconTheme: IconThemeData(
                        color: Color.fromARGB(255, 255, 131, 181))),
                child: profileActionItems()),
          ),
          Divider(),
          mobilePhoneListTile(),
          otherPhoneListTile(),
          emailListTile(),
          addressListTile(),
        ],
      ),
    ],
  );
}

Widget buildCallButton() {
  return Column(
    children: <Widget>[
      IconButton(
        icon: Icon(
          Icons.call,
        ),
        onPressed: () {},
      ),
      Text("Call"),
    ],
  );
}

Widget buildTextButton() {
  return Column(
    children: <Widget>[
      IconButton(
        icon: Icon(
          Icons.message,
        ),
        onPressed: () {},
      ),
      Text("Text"),
    ],
  );
}

Widget buildVideoCallButton() {
  return Column(
    children: <Widget>[
      IconButton(
        icon: Icon(
          Icons.video_call,
        ),
        onPressed: () {},
      ),
      Text("Video"),
    ],
  );
}

Widget buildEmailButton() {
  return Column(
    children: <Widget>[
      IconButton(
        icon: Icon(
          Icons.email,
        ),
        onPressed: () {},
      ),
      Text("Pay"),
    ],
  );
}

Widget buildDirectionsButton() {
  return Column(
    children: <Widget>[
      IconButton(
        icon: Icon(
          Icons.directions,
        ),
        onPressed: () {},
      ),
      Text("Directions"),
    ],
  );
}

Widget buildPayButton() {
  return Column(
    children: <Widget>[
      IconButton(
        icon: Icon(
          Icons.attach_money,
        ),
        onPressed: () {},
      ),
      Text("Pay"),
    ],
  );
}

Widget mobilePhoneListTile() {
  return ListTile(
    leading: Icon(Icons.call),
    title: Text("062-834-6527"),
    subtitle: Text("mobile"),
    trailing: IconButton(
      icon: Icon(
        Icons.message,
        color: Color.fromARGB(255, 255, 131, 181),
      ),
      onPressed: () {},
    ),
  );
}

Widget otherPhoneListTile() {
  return ListTile(
    leading: Text(""),
    title: Text("082-258-9627"),
    subtitle: Text("other"),
    trailing: IconButton(
      icon: Icon(
        Icons.message,
        color: Color.fromARGB(255, 255, 131, 181),
      ),
      onPressed: () {},
    ),
  );
}

Widget emailListTile() {
  return ListTile(
    leading: Icon(Icons.email),
    title: Text("63160208@go.buu.ac.th"),
    subtitle: Text("work"),
  );
}

Widget addressListTile() {
  return ListTile(
    leading: Icon(Icons.location_on),
    title: Text("499/138 The Centro BKK"),
    subtitle: Text("home"),
    trailing: IconButton(
      icon: Icon(
        Icons.directions,
        color: Color.fromARGB(255, 255, 131, 181),
      ),
      onPressed: () {},
    ),
  );
}

Widget profileActionItems() {
  return Row(
    mainAxisAlignment: MainAxisAlignment.spaceEvenly,
    children: <Widget>[
      buildCallButton(),
      buildTextButton(),
      buildVideoCallButton(),
      buildEmailButton(),
      buildDirectionsButton(),
      buildPayButton()
    ],
  );
}
